const targetTime = new Date(2020, 2, 7, 17, 0);

window.addEventListener('load', () => {
    let countdown = document.getElementById('countdown');

    let interval = window.setInterval(() => {
        let nowTime = new Date()
        let diff = targetTime - nowTime

        if (diff < 0) {
            countdown.innerText = "Time Up!"
            window.clearInterval(interval)
        } else {
            countdown.innerText = timeDiff(diff);
        }
    }, 500)
})

function timeDiff(diff) {
    let msec = diff;
    let hh = Math.floor(msec / 1000 / 60 / 60);
    msec -= hh * 1000 * 60 * 60;
    let mm = Math.floor(msec / 1000 / 60);
    msec -= mm * 1000 * 60;
    let ss = Math.floor(msec / 1000);
    msec -= ss * 1000;

    return hh.toString().padStart(2, '0') + ":" + mm.toString().padStart(2, '0') + ":" + ss.toString().padStart(2, '0');
}